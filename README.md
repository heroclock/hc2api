# HC2API

## Description

HP2API is the backend for the web application [HeroClockr2](https://gitlab.com/heroclock/heroclock2). Please see the [Readme](https://gitlab.com/heroclock/heroclock2/-/blob/main/README.md) there for further description
The backend is set up with

- [Node.js](https://nodejs.org/en)
- [Express.js](https://expressjs.com/)

## Usage

With the right credentials the backend can be used like an API.

### Privacy

The stack itself is just meant to showcase and does not feature constantly evolving security measures

## Support

Feel free to contact me here on [GitLab](https://gitlab.com/cyaton).

## Feedback

Any feedback, suggestions or proper collaboration attempts are very welcome! Especially for other projects!


## Authors and acknowledgment

Contributors: Philipp Montazem

### Acknowledgments

#### Manik

Thanks to [Manik](https://cloudaffle.com/) for providing the [TypeScript Complete Course](https://www.udemy.com/course/typescript-course/) which heavily inspired this project.

#### Mozilla

As always, thanks to Mozilla for providing the [mdn resources for developers](https://developer.mozilla.org/en-US/).

## LICENSE

Copyright (C) 2023 Philipp Montazem

This Software is published under the GNU Affero General Public License v3.0. Please see [LICENSE.md](LICENSE.md) for this softwares specific license conditions.

## Project status

Project finished. Readme last updated on September 2023

