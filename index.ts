// Basic app structure
// IN: Request → Router/validation → Controller → ORM → DB
// OUT: DB → ORM → Controller → Response

// Import with 3 interfaces Express, Request and Response
import express, {Express} from "express";

// Import typeORM
import {DataSource} from "typeorm";

import dotenv from "dotenv";
import cors from 'cors';
import bodyParser from "body-parser";
import {Streak} from "./src/streaks/streaks.entity";
import {User} from "./src/users/user.entity";
import {streaksRouter} from "./src/streaks/streaks.router";
import {userRouter} from "./src/users/user.router";

// Instanciate express app
const app: Express = express();
dotenv.config();

// Parse request Body
app.use(bodyParser.json());

// Use CORS with installed types
app.use(cors());

// Database connection
export const AppDataSource = new DataSource({
    type: 'mysql',
    host: 'localhost',
    port: 3306,
    username: process.env.MYSQL_USER,
    password: process.env.MYSQL_PASSWORD,
    database: process.env.MYSQL_DB,
    entities: [Streak, User],
    synchronize: true,
});

// Server port
const port = process.env.PORT;

// Init typeORM
AppDataSource.initialize().then(() => {
    // Start listening to requests
    app.listen(port);
    console.log('Data Source is up and running');
}).catch((err) => {
    console.error(
        'Something went wrong, Houston, the data source could not be initialized',
        err,
    )
});

app.use('/', streaksRouter);
app.use('/', userRouter);