import {AppDataSource} from "../../index";
import {User} from "./user.entity";
import {instanceToPlain} from "class-transformer";
import {Request, Response} from "express";
import {validationResult} from "express-validator";

class UserController {

    // Method for get route
    public async getAll(req: Request, res: Response): Promise<Response> {
        // Declare variable for streaks
        let allUsers: User[];

        // Fetch all streaks
        try {
            allUsers = await AppDataSource.getRepository(User).find({
                order: {
                    name: 'ASC',
                }
            });

            // Convert streaks to array of objects
            allUsers = instanceToPlain(allUsers) as User[];

            return res.json(allUsers).status(200);
        } catch (_errors) {
            return res
                .json({error: 'Internal Server Error'})
                .status(500);
        }
    }

    // Method for post route
    public async create(req: Request, res: Response): Promise<Response> {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }

        // Create new User instance
        const newUser = new User();

        // Add required properties to User object
        newUser.name = req.body.name;
        newUser.email = req.body.email;


        // Save User to database
        let createdUser: User;
        try {
            createdUser = await AppDataSource.getRepository(
                User,
            ).save(newUser);

            // Convert instance to plain object
            createdUser = instanceToPlain(createdUser) as User;

            return res.json(createdUser).status(201);
        } catch (errors){
            return res
                .json({error: 'Internal Server Error'})
                .status(500);
        }
    }
}

export const userController = new UserController();