import {Column, Entity, PrimaryGeneratedColumn} from "typeorm";

@Entity()
export class User {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column( {
        type: 'varchar',
        length: 254,
        unique: true,
    })
    name: string;

    @Column( {
        type: 'varchar',
        length: 254,
        unique: true,
    })
    email: string;
}