import {body, ValidationChain} from "express-validator";

export const createUserValidator : ValidationChain[] = [
    body('name')
        .not()
        .isEmpty()
        .withMessage('A user name is mandatory')
        .trim()
        .isString()
        .withMessage('The user name needs to be in text format.')
        .isLength({min: 3})
        .withMessage('The username is too short.'),
    body('email')
        .isEmail()
        .withMessage('A valid email adress is required'),
];