import {Router} from "express";
import {userController} from "./user.controller";
import {createUserValidator} from "./user.validator";

// Trigger router function
export const userRouter: Router = Router();

// Default route
userRouter.get('/user', userController.getAll);

userRouter.post(
    '/user',
    createUserValidator,
    userController.create
);