// "strictPropertyInitialization" deactivated in tsconfig.json for this class -> never instanciated
// Also: "experimentalDecorators" and "emitDecoratorMetadata" set to true for TypeORM
// Remember to startup database and server when usage of synchronize is desired
// PrimaryGeneratedColumn for UID

import {Entity, PrimaryGeneratedColumn, Column, ManyToOne} from "typeorm";
import {User} from "../users/user.entity";

// TypeORM exports Entity as a decorator factory
@Entity()
export class Streak {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ManyToOne(() => User, (user) => user.id )
    user: User;

    @Column( {
        type: 'varchar',
        length: 254,
        unique: true,
    })
    title: string;

    @Column( {
        type: 'varchar',
        length: 254,
    })
    description: string;

    @Column({
        type: 'date',
    })
    dateStarted: string;

    @Column({
        type: 'smallint',
        default: 0,
    })
    attempts: number
}