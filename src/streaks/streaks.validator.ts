import {body, ValidationChain} from "express-validator";

export const createStreakValidator : ValidationChain[] = [
    body('title')
        .not()
        .isEmpty()
        .withMessage('The streak title is mandatory')
        .trim()
        .isString()
        .withMessage('The title needs to be in text format.'),
    body('description')
        .isString()
        .withMessage('The description needs to be in text format.'),
    body('dateStarted')
        .isString()
        .withMessage('The start date needs to be in text format.'),
];

export const updateStreakValidator = [
    body('id')
        .not()
        .isEmpty()
        .withMessage('Streak id is mandatory')
        .trim()
        .isString()
        .withMessage('ID needs to be a valid uuid format'),
]