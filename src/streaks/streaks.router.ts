import {Router} from "express";
import {streaksController} from "./streaks.controller";
import {createStreakValidator, updateStreakValidator} from "./streaks.validator";

// Trigger router function
export const streaksRouter: Router = Router();

// Default route
streaksRouter.get('/streaks', streaksController.getAll);

streaksRouter.post(
    '/streaks',
    createStreakValidator,
    streaksController.create
);

streaksRouter.put(
    '/streaks',
    updateStreakValidator,
    streaksController.update
);

streaksRouter.delete(
    '/streaks',
    updateStreakValidator,
    streaksController.delete
);