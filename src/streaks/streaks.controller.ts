import {AppDataSource} from "../../index";
import {Streak} from "./streaks.entity";
import {instanceToPlain, plainToInstance} from "class-transformer";
import {Request, Response} from "express";
import {validationResult} from "express-validator";
import {UpdateResult, DeleteResult} from "typeorm";

class StreaksController {

    // Method for get route
    public async getAll(req: Request, res: Response): Promise<Response> {
        // Declare variable for streaks
        let allStreaks: Streak[];

        // Fetch all streaks
        try {
            allStreaks = await AppDataSource.getRepository(Streak).find({
                where: {user: req.body.user},
                order: {
                    dateStarted: 'ASC',
                }
            });

            // Convert streaks to array of objects
            allStreaks = instanceToPlain(allStreaks) as Streak[];

            return res.json(allStreaks).status(200);
        } catch (_errors) {
           return res
               .json({error: 'Internal Server Error'})
               .status(500);
        }
    }

    // Method for post route
    public async create(req: Request, res: Response): Promise<Response> {
        const errors = validationResult(req);

        if (!errors.isEmpty()) {
            return res.status(400).json({errors: errors.array()});
        }

        // Create new Streak instance
        const newStreak = new Streak();

        // Add required properties to Streak object
        newStreak.user = req.body.user;
        newStreak.title = req.body.title;
        newStreak.description = req.body.description;
        newStreak.dateStarted = req.body.dateStarted;


        // Save Streak to database
        let createdStreak: Streak;
        try {
            createdStreak = await AppDataSource.getRepository(
                Streak,
            ).save(newStreak);

            // Convert instance to plain object
            createdStreak = instanceToPlain(createdStreak) as Streak;

            return res.json(createdStreak).status(201);
        } catch (errors){
            return res
                .json({error: 'Internal Server Error'})
                .status(500);
        }
    }

    // Update method
    public async update(req: Request, res: Response): Promise<Response> {
        const errors = validationResult(req);

        if(!errors.isEmpty()) {
            return res
                .status(400)
                .json({errors: errors.array()});
        }

        // Find streak
        let streak: Streak | null;

        try {
            streak = await  AppDataSource.getRepository((Streak)).findOne({
                where: { id: req.body.id},
            });
        } catch (errors) {
            return res
                .json({error: 'Internal Server Error, not Found'})
                .status(500);
        }

        // Return 400 if streak is null
        if(!streak) {
            return res.status(404).json({
                error: 'The streak with given ID does not exist',
            })
        }

        // Declare variable for updatedStreak
        let updatedStreak: UpdateResult;

        // Update streak
        try {
            updatedStreak = await  AppDataSource.getRepository(
                Streak,
            ).update(req.body.id, plainToInstance(Streak, {
                dateStarted: req.body.dateStarted,
            }));

            // Convert updatedStreak instance to plain object
            updatedStreak = instanceToPlain(
                updatedStreak,
            ) as UpdateResult;

            return res.json(updatedStreak).status(200);
        } catch (errors) {
            return res
                .json({error: 'Internal Server Error, not returned:' + errors})
                .status(500);
        }

        // Convert updatedStreak instance to object
    }

    // Method for delete route
    public async delete(req: Request, res: Response): Promise<Response> {
       const errors = validationResult(req);

        if(!errors.isEmpty()) {
            return res
                .status(400)
                .json({errors: errors.array()});
        }

        // Find streak
        let streak: Streak | null;

        try {
            streak = await  AppDataSource.getRepository((Streak)).findOne({
                where: { id: req.body.id},
            });
        } catch (errors) {
            return res
                .json({error: 'Internal Server Error, not Found'})
                .status(500);
        }

        // Return 400 if streak is null
        if(!streak) {
            return res.status(404).json({
                error: 'The streak with given ID does not exist',
            })
        }

        let deletedStreak: DeleteResult;

        // Update streak
        try {
            deletedStreak = await  AppDataSource.getRepository(
                Streak,
            ).delete(req.body.id);

            // Convert updatedStreak instance to plain object
            deletedStreak = instanceToPlain(
                deletedStreak,
            ) as DeleteResult;

            return res.json(deletedStreak).status(200);
        } catch (errors) {
            return res
                .json({error: 'Internal Server Error, not returned:' + errors})
                .status(500);
        }
    }
}

export const streaksController = new StreaksController();